// GENERATED AUTOMATICALLY FROM 'Assets/InputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputActions"",
    ""maps"": [
        {
            ""name"": ""PlayerMovementMap"",
            ""id"": ""4dc6217a-9c88-4d5e-a0d2-9d67cd63d802"",
            ""actions"": [
                {
                    ""name"": ""MoveAction"",
                    ""type"": ""Value"",
                    ""id"": ""a6894a4d-0635-4d06-ab1c-221197d2b900"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AttackAction"",
                    ""type"": ""Button"",
                    ""id"": ""62e3f1d6-8fce-4219-9681-8ff1e90468e0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""9cb94b84-b2b9-4e6a-8198-efe82d946101"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveAction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""WASD Keys"",
                    ""id"": ""216b9046-21e1-462a-b04f-38f2623d3a56"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveAction"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""0961efe4-9610-4bd2-b715-a5696374f159"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveAction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""5bbb023b-8e89-4869-9f4c-e68939cfba52"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveAction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""b95b5481-b653-4dfe-b8b0-71f4ff7d80fb"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveAction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""0c3530de-02b7-4c09-a2c9-3c5e9287866a"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveAction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""c5e48c02-f4ae-48fd-83bf-f60afbe798cd"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AttackAction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // PlayerMovementMap
        m_PlayerMovementMap = asset.FindActionMap("PlayerMovementMap", throwIfNotFound: true);
        m_PlayerMovementMap_MoveAction = m_PlayerMovementMap.FindAction("MoveAction", throwIfNotFound: true);
        m_PlayerMovementMap_AttackAction = m_PlayerMovementMap.FindAction("AttackAction", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // PlayerMovementMap
    private readonly InputActionMap m_PlayerMovementMap;
    private IPlayerMovementMapActions m_PlayerMovementMapActionsCallbackInterface;
    private readonly InputAction m_PlayerMovementMap_MoveAction;
    private readonly InputAction m_PlayerMovementMap_AttackAction;
    public struct PlayerMovementMapActions
    {
        private @InputActions m_Wrapper;
        public PlayerMovementMapActions(@InputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @MoveAction => m_Wrapper.m_PlayerMovementMap_MoveAction;
        public InputAction @AttackAction => m_Wrapper.m_PlayerMovementMap_AttackAction;
        public InputActionMap Get() { return m_Wrapper.m_PlayerMovementMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerMovementMapActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerMovementMapActions instance)
        {
            if (m_Wrapper.m_PlayerMovementMapActionsCallbackInterface != null)
            {
                @MoveAction.started -= m_Wrapper.m_PlayerMovementMapActionsCallbackInterface.OnMoveAction;
                @MoveAction.performed -= m_Wrapper.m_PlayerMovementMapActionsCallbackInterface.OnMoveAction;
                @MoveAction.canceled -= m_Wrapper.m_PlayerMovementMapActionsCallbackInterface.OnMoveAction;
                @AttackAction.started -= m_Wrapper.m_PlayerMovementMapActionsCallbackInterface.OnAttackAction;
                @AttackAction.performed -= m_Wrapper.m_PlayerMovementMapActionsCallbackInterface.OnAttackAction;
                @AttackAction.canceled -= m_Wrapper.m_PlayerMovementMapActionsCallbackInterface.OnAttackAction;
            }
            m_Wrapper.m_PlayerMovementMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @MoveAction.started += instance.OnMoveAction;
                @MoveAction.performed += instance.OnMoveAction;
                @MoveAction.canceled += instance.OnMoveAction;
                @AttackAction.started += instance.OnAttackAction;
                @AttackAction.performed += instance.OnAttackAction;
                @AttackAction.canceled += instance.OnAttackAction;
            }
        }
    }
    public PlayerMovementMapActions @PlayerMovementMap => new PlayerMovementMapActions(this);
    public interface IPlayerMovementMapActions
    {
        void OnMoveAction(InputAction.CallbackContext context);
        void OnAttackAction(InputAction.CallbackContext context);
    }
}
