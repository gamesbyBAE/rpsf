using UnityEngine;

public class StopHUDRotation : MonoBehaviour
{
    Quaternion originalRotation;
    private void Awake()
    {
        originalRotation = transform.rotation;
    }
    void LateUpdate()
    {
        transform.rotation = originalRotation;
    }
}
