using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;

public class PlayerHUD : NetworkBehaviour
{
    [SerializeField] GameObject playerIndicator = default;
    [SerializeField] Image healthBar = default;

    public override void OnStartLocalPlayer()
    {
        playerIndicator.SetActive(true);
    }

    void UpdateHealthBar()
    {

    }
}
