using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class MyPlayerController : NetworkBehaviour
{
    //[SerializeField] VoidEventChannelSO spacePressed = default;

    private CharacterController controller;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    [SerializeField] private float playerSpeed = 2.0f;
    [SerializeField] private float jumpHeight = 1.0f;
    [SerializeField] private float gravityValue = -9.81f;

    InputActions inputActions;
    Animator playerAnimator;
    void Awake()
    {
        inputActions = new InputActions();
        playerAnimator = GetComponent<Animator>();
        controller = gameObject.GetComponent<CharacterController>();
    }

    private void OnEnable()
    {
        inputActions.Enable();
    }

    private void OnDisable()
    {
        inputActions.Disable();
    }

    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        Vector2 playerMovement = inputActions.PlayerMovementMap.MoveAction.ReadValue<Vector2>();
        Vector3 move = new Vector3(playerMovement.x, 0, playerMovement.y);
        controller.Move(move * Time.deltaTime * playerSpeed);

        if (move != Vector3.zero)
        {
            gameObject.transform.forward = move;
        }
        playerAnimator.SetBool("isWalking", move != Vector3.zero);
        // Player attacks on pressing the attack button.
        // Currently using it to make the cube jump.
        if (inputActions.PlayerMovementMap.AttackAction.triggered && groundedPlayer)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
            //spacePressed.RaiseEvent();
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);
    }
}
