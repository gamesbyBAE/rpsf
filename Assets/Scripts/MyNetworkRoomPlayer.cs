using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.SceneManagement;

public class MyNetworkRoomPlayer : NetworkRoomPlayer
{
    IEnumerator waitCoroutine;
    WaitForSeconds waitTime = new WaitForSeconds(0.02f);
    
    // For some reason, "Online Scene" is being called in OnClientExitRoom after changing from "Lobby Scene".
    // OnClientEntryRoom doesn't set the condition true.
    public override void OnClientExitRoom()
    {
        if (isLocalPlayer && SceneManager.GetActiveScene().name == "Online Scene")
        {
            waitCoroutine = WaitSome();
            StartCoroutine(waitCoroutine);
        }
    }

    // Using a coroutine to create a delay, so that sufficient time passes to run Commands.
    // Else throws error.
    IEnumerator WaitSome()
    {
        while (!NetworkClient.ready)
        {
            yield return waitTime;
        }
        StopCoroutine(waitCoroutine);
        CmdDestroy();
    }

    // Using NetworkServer.Destroy(roomPlayer) in OnRoomServerSceneLoadedForPlayer method of MyNetworkRoomManager class,
    // removes all the RoomPlayer GOs from the Host and in Clients, removes only the local RoomPlayer game object and not the
    // other RoomPlayer GOs.
    //
    // Using Command to destroy because no option to override Start method of the parent, NetworkRoomPlayer.
    // It uses DontDestroyOnLoad in it's Start method to make the lobby/room players persistent throughout the scenes.
    //
    // Would've worked out of the box if we didn't let the players to roam around inside the lobby. Therefore, a stripped
    // down version of Player prefab is used as RoomPlayer prefab to spawn in Lobby/Room and controlled by the player.
    // "Online Scene" spawns the full-fledged Player prefab, thus, RoomPlayer game objects has to be destroyed.
    [Command]
    void CmdDestroy()
    {
        RpcDestroy();
    }

    [ClientRpc]
    void RpcDestroy()
    {
        Destroy(this.gameObject);
    }
}
