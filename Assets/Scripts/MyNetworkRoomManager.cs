using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class MyNetworkRoomManager : NetworkRoomManager
{
    bool showStartButton;

    public override bool OnRoomServerSceneLoadedForPlayer(NetworkConnection conn, GameObject roomPlayer, GameObject gamePlayer)
    {
        gamePlayer.transform.GetChild(1).GetComponentInChildren<Renderer>().material = roomPlayer.transform.GetChild(1).GetComponentInChildren<Renderer>().material;
        return true;
    }

    public override void OnRoomServerPlayersReady()
    {
        // calling the base method calls ServerChangeScene as soon as all players are in Ready state.
    #if UNITY_SERVER
            base.OnRoomServerPlayersReady();
    #else
        showStartButton = true;
    #endif
    }

    public override void OnGUI()
    {
        base.OnGUI();

        if (allPlayersReady && showStartButton && GUI.Button(new Rect(150, 300, 120, 20), "START GAME"))
        {
            // set to false to hide it in the game scene
            showStartButton = false;

            ServerChangeScene(GameplayScene);
        }
    }
}
