using UnityEngine;
using Mirror;

/// <summary>
/// � Changes the color/material of the player after being spawned.
/// � Raises a request to get the index from the game object that
///   manages the index to be sent to various players.
/// � On receiving the index, applies the material from the material list
///   at that particular index.
/// </summary>
public class ChangePlayerColor : NetworkBehaviour
{ 
    // Scriptable Object asset that is the list of materials.
    [SerializeField] PlayerMaterialsListSO playerMaterialsList = default;

    [Header("Event Channels")]
    [SerializeField] NetworkConnectionEventChannelSO playerNetworkConnection = default;

    [SyncVar(hook = nameof(ChangeColor))]
    int playerMaterialIndex = -1;

    // � Unity makes a clone of the Material every time GetComponent().material is used.
    //   Cache it here and Destroy it in OnDestroy to prevent a memory leak.
    // � Copied the above lines from Mirror documentation.(Applies for Renderer as well?)
    //   (https://mirror-networking.gitbook.io/docs/guides/synchronization/syncvar-hooks)
    [SerializeField] Renderer playerRenderer = default;

    public override void OnStartClient()
    {
        if (!isLocalPlayer) { return; }

        CmdRequestIndex();
    }

    // � Raising the event (requesting index) to server.
    // � Because the game object with the script, ColorIndex.cs
    //   which sends the index, resides only on the server.
    [Command]
    void CmdRequestIndex()
    {
        playerNetworkConnection.RaiseEvent(connectionToClient);
    }

    // TargetRpc, so that only the client that requested receives
    // the index from the server.
    [TargetRpc]
    public void TargetReceiveIndex(NetworkConnection nc, int index)
    {
        CmdUpdateIndex(index);
    }

    // � Updates it's value to the received index value.
    // � And notifying the server of it as well so that every client can sync.
    [Command]
    void CmdUpdateIndex(int val)
    {
        playerMaterialIndex = val;
    }

    // � Called automatically when value of playerMaterialIndex changes because
    //   of the hook attribute used during declaration.
    // � Gets the Renderer component attached on the Player prefab and assigns
    //   the new material to it.
    void ChangeColor(int oldVal, int newVal)
    {
        if (playerRenderer == null)
        {
            playerRenderer = this.gameObject.GetComponent<Renderer>();
        }
        playerRenderer.material = playerMaterialsList.GetMaterial(newVal);
    }

    private void OnDestroy()
    {
        Destroy(playerRenderer);
    }
}
