using UnityEngine;

/// <summary>
/// List to hold different materials that players prefab can apply on itself.
/// </summary>
[CreateAssetMenu(fileName = "New Player Materials List", menuName = "Player Materials List", order = 51)]
public class PlayerMaterialsListSO : ScriptableObject
{
    [SerializeField] Material[] playerMaterials;

    // Getter to return total number of materials in the list.
    public int MaterialListCount { get { return playerMaterials.Length; } }

    // Returns the material from the list at a particular index.
    public Material GetMaterial(int value)
    {
        return playerMaterials[value];
    }
}
