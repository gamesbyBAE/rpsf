using UnityEngine;
using Mirror;

/// <summary>
/// � Calculates the index value to be sent to the requesting player/client.
/// � Used on the game object that runs only on the server.
/// </summary>

// TO-DO:
// � Dynamically Spawn the game object this script is attached on, only on the server.
//   This might lead to inheriting again from NetworkBehaviour and require the NetworkIdentity component.
// � Let the game object be in scene already, which means, available on clients also.
//   But only the game object on server executes this class.
public class ColorIndex : MonoBehaviour
{
    [SerializeField] PlayerMaterialsListSO playerMaterialsList = default;

    [Header("Event Channels")]
    [SerializeField] NetworkConnectionEventChannelSO playerNCChannel = default;

    int count;
    int totalCount;
    void Awake()
    {
        count = -1;
        totalCount = playerMaterialsList.MaterialListCount;
    }

    private void OnEnable()
    {
        playerNCChannel.OnNetworkConnectionEventRaised += CalculateIndexVal;
    }
    private void OnDisable()
    {
        playerNCChannel.OnNetworkConnectionEventRaised -= CalculateIndexVal;
    }

    [Server]
    void CalculateIndexVal(NetworkConnection nc)
    {
        count = count < totalCount ? count + 1 : count;

        // � Tries to get the ChangePlayerColor script attached on the Player prefab.
        // � Then calls it's public method, TargetReceiveIndex.
        if (!nc.identity.TryGetComponent<ChangePlayerColor>(out ChangePlayerColor cpc))
        {
            return;
        }
        cpc.TargetReceiveIndex(nc, count);
    }
}
