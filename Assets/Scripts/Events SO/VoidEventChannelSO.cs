using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class is used for Events that have no argument.
/// Example: Raise an event when Space Bar is pressed.
/// </summary>
[CreateAssetMenu(fileName = "New Void Event Channel", menuName = "Events/Void Event Channel")]
public class VoidEventChannelSO : ScriptableObject
{
    public UnityAction OnVoidEventRaised;
    
    public void RaiseEvent()
    {
        if (OnVoidEventRaised != null)
        {
            OnVoidEventRaised.Invoke();
        }
        else
        {
            Debug.Log("No one subscribed to this Void Event Channel");
        }
    }
}
