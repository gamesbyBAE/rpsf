using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class is used for Events that has UInt as argument.
/// Example: Sending game object's netid.
/// </summary>
[CreateAssetMenu(fileName = "New UInt Event Channel", menuName = "Events/UInt Event Channel")]
public class UIntEventChannelSO : ScriptableObject
{
    public UnityAction<uint> OnUIntEventRaised;

    public void RaiseEvent(uint value)
    {
        if (OnUIntEventRaised != null)
        {
            OnUIntEventRaised.Invoke(value);
        }
        else
        {
            Debug.Log("No one subscribed to this UInt Event Channel");
        }
    }
}
