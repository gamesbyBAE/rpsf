using UnityEngine;
using UnityEngine.Events;
using Mirror;

/// <summary>
/// This class is used for Events that have one NetworkConnection argument.
/// Example: To raise event with the player's network connection related info
/// like it's connection ID, so that the color index can be sent to only that client.
/// </summary>
[CreateAssetMenu(fileName = "New NetworkConnection Event Channel", menuName = "Events/NetworkConnection Event Channel")]
public class NetworkConnectionEventChannelSO : ScriptableObject
{
    public UnityAction<NetworkConnection> OnNetworkConnectionEventRaised;

    public void RaiseEvent(NetworkConnection val)
    {
        if (OnNetworkConnectionEventRaised != null)
        {
            OnNetworkConnectionEventRaised.Invoke(val);
        }
        else
        {
            Debug.Log("No one subscribed to this NetworkConnection Event Channel");
        }
    }
}
