using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum AbilityTypeEnum
{
    Armor,
    SpeedBoost,
    Heal,
    SpeedDamp,
    TimeBomb,
    SloMo,
}

/// <summary>
/// A dictionary to map AbilityType enum to respective AbilityDescription SO assets.
/// Also, holds a sorted array of different abilities according to decreasing SpawnPriority values.
/// </summary>
public class GameDataSource : MonoBehaviour
{
    [SerializeField] AbilityDescriptionSO[] abilityData;
    Dictionary<AbilityTypeEnum, AbilityDescriptionSO> abilityDataMap;

    public static GameDataSource Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            throw new System.Exception("Multiple GameDataSources defined!");
        }

        DontDestroyOnLoad(gameObject);
        Instance = this;

        // � Sorts the array in decreasing value of SpawnPriority and stores in itself.
        //   Necessary, primarily for AbilitiesSpawner's GetAbilityType method.
        // � This requires usage of System.Linq
        abilityData = abilityData.OrderByDescending(ad => ad.SpawnPriority).ToArray();
    }

    public Dictionary<AbilityTypeEnum, AbilityDescriptionSO> AbilityDataByType
    {
        get
        {
            // Populating the dictionary if empty.
            if (abilityDataMap == null)
            {
                abilityDataMap = new Dictionary<AbilityTypeEnum, AbilityDescriptionSO>();
                foreach (AbilityDescriptionSO data in abilityData)
                {
                    if (abilityDataMap.ContainsKey(data.AbilityTypeEnum))
                    {
                        throw new System.Exception($"Duplicate ability definition detected: {data.AbilityTypeEnum}");
                    }
                    abilityDataMap[data.AbilityTypeEnum] = data;
                }
            }

            return abilityDataMap;
        }
    }

    public AbilityDescriptionSO[] SortedAbilityArray
    {
        get
        {
            return abilityData;
        }
    }
}
