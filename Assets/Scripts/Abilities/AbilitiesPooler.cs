using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class AbilitiesPooler : NetworkBehaviour
{
    //[SerializeField] IntEventChannelSO abilityEnumToSpawnChannel = default;
    [SerializeField] VoidEventChannelSO abilityTypeUpdatedEvent = default;
    [SerializeField] GameObject abilityPrefab;

    Queue<GameObject>[] pool;

    AbilityTypeEnum currentAbilityType;
    
    void Start()
    {
        InitialisePool();

        // � To register functions to spawn and destroy client game objects.
        //   The server creates game objects directly, and then spawns them on
        //   the clients through this functionality.
        // � Register prefab you'd like to custom spawn and pass in handlers.
        NetworkClient.RegisterPrefab(abilityPrefab, SpawnHandler, UnspawnHandler);
    }

    private void OnDestroy()
    {
        NetworkClient.UnregisterPrefab(abilityPrefab);
    }

    void InitialisePool()
    {
        // Specifying total length of the pool array which holds
        // queues of different abilities at each index.
        pool = new Queue<GameObject>[GameDataSource.Instance.AbilityDataByType.Count];

        // For each value of AbilityTypeEnum, we create a new Queue for that respective
        // ability and populate it with the game object of that type
        // at an index of pool array which we get by typecasting AbilityTypeEnum as int.
        foreach (AbilityTypeEnum ability in System.Enum.GetValues(typeof(AbilityTypeEnum)))
        {
            currentAbilityType = ability;
            pool[(int)currentAbilityType] = new Queue<GameObject>();
            for (int i = 0; i < GameDataSource.Instance.AbilityDataByType[currentAbilityType].MinCopies; i++)
            {
                GameObject next = CreateNew(i);
                pool[(int)currentAbilityType].Enqueue(next);
            }
        }

        // Resetting it back to 0 after populating the queues.
        currentAbilityType = 0; 
    }

    GameObject CreateNew(int cloneNumber)
    {
        AbilityDescriptionSO retrievedFromDict = GameDataSource.Instance.AbilityDataByType[currentAbilityType];
        
        GameObject temp = Instantiate(abilityPrefab, transform);
        temp.name = $"{currentAbilityType}_Clone{cloneNumber}";
        temp.GetComponent<Renderer>().material = retrievedFromDict.AbilityMaterial;
        temp.GetComponent<BaseAbility>().AbilityType = currentAbilityType;
        //remove this if check once place holder ability icons are in place.
        if (retrievedFromDict.AbilitySprite != null)
        {
            temp.GetComponentInChildren<SpriteRenderer>().sprite = retrievedFromDict.AbilitySprite;
        }
        temp.SetActive(false);

        return temp;
    }

    // NetworkServer.Spawn(gameobject) calls this method.
    // Used to spawn object on client when Server gets an object from the pool.
    GameObject SpawnHandler(SpawnMessage msg)
    {
        return GetFromPool(msg.position);
    }

    // NetworkServer.UnSpawn(gameobject) calls this method.
    // Used to unspawn object on client when Server puts an object back into the pool.
    void UnspawnHandler(GameObject spawned)
    {
        PutBackInPool(spawned);
    }

    public GameObject GetFromPool(Vector3 position)
    {
        GameObject next = pool[(int)currentAbilityType].Count > 0 ? pool[(int)currentAbilityType].Dequeue() : null;

        if(next == null) { return null; }

        next.transform.position = position;
        next.SetActive(true);
        return next;
    }

    public void PutBackInPool(GameObject spawned)
    {
        spawned.SetActive(false);
        pool[(int)spawned.GetComponent<BaseAbility>().AbilityType].Enqueue(spawned);
    }

    [ClientRpc]
    public void RpcUpdateCurrentAbility(AbilityTypeEnum val)
    {
        currentAbilityType = val;

        // � There was a sync delay between value of currentAbilityType being updated on clients and
        //   server AbilitiesSpawner requesting a game object of that type.
        //   This lead to different ability being spawned on server(host player) and clients(joined players).
        // 
        //   To fix this, we are raising the event when the currentAbilityType value has been updated, so that
        //   the server requests for a gameobject only then.
        // � Since, AbilitiesSpawner.cs runs only on Server. No Client's AbilitiesSpawner.cs would be subscribed
        //   to the event, perhaps, no need for it to run on clients.
        if (isClientOnly) { return; }

        abilityTypeUpdatedEvent.RaiseEvent();
    }
}
