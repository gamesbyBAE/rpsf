using System.Collections;
using UnityEngine;
using Mirror;

public class DisableAbility : NetworkBehaviour
{
    [Tooltip("Time in seconds after which to unspawn an ability if no one picks it.")]
    [SerializeField] float autoDisableDuration = default;
    
    [Header("Event Channel")]
    [SerializeField] UIntEventChannelSO abilityNetID = default;

    IEnumerator disableTimer;
    WaitForSeconds waitTime;

    private void Awake()
    {
        waitTime = new WaitForSeconds(autoDisableDuration);
    }

    public override void OnStartServer()
    {
        disableTimer = SelfDisableCountdown();
        StartCoroutine(disableTimer);
    }

    // Automatically raise an event to unspawn after mentioned time if no one picks the ability.
    IEnumerator SelfDisableCountdown()
    {
        yield return waitTime;
        abilityNetID.RaiseEvent(this.gameObject.GetComponent<NetworkIdentity>().netId);
        StopCoroutine(disableTimer);
    }
}
