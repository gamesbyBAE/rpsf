using System.Collections;
using UnityEngine;
using Mirror;

public class AbilitiesSpawner : NetworkBehaviour
{
    [Header("Event Channels")]
    [SerializeField] VoidEventChannelSO abilityTypeUpdatedChannel = default;
    [SerializeField] UIntEventChannelSO abilityToDisableChannel = default;
    
    // Spawning
    [Header("Spawning")]
    [Tooltip("Maximum no of abilities that should be allowed on the arena at once.")]
    [SerializeField] int maxAbilitiesActive = default;

    [Tooltip("Time interval after which to spawn a new ability.")]
    [SerializeField] float timeInterval = default;
    WaitForSeconds waitTime;
    WaitUntil waitUntilReadyToSpawn;

    // Spawn Region
    [Header("Spawn Region")]
    [SerializeField] Collider groundGameObject = default;

    [Tooltip("x-value to subtract from ground's width(total x-distance), to avoid spawning at edges of the ground.")]
    [SerializeField] float xOffset = default;

    [Tooltip("z-value to subtract from ground's width(total z-distance), to avoid spawning at edges of the ground.")]
    [SerializeField] float zOffset = default;
    float xPosMin, xPosMax;
    float zPosMin, zPosMax;

    [Tooltip("Radius to create a sphere which checks for empty space suitable for spawning.")]
    [SerializeField] float spawnPositionCheckerRadius = default;

    [Tooltip("Layer which the Spawn Position Checker checks for collision with.")]
    [SerializeField] LayerMask collisionLayerMask = default;

    [Tooltip("No. of times loop checks for new position, in case it overlaps with other objects to avoid being stuck in an infinite loop.")]
    [SerializeField] int maxPositionCheckerQuery = default;


    AbilitiesPooler abilitiesPooler;

    IEnumerator spawnCoroutine;

    int activeAbilitiesCount;

    bool readyToSpawn;

    int[] prefixSumSpawnPriority;

    void Start()
    {
        // Run only if it is the host/server.
        if (!isServer) { return; } 

        abilitiesPooler = GetComponent<AbilitiesPooler>();
        activeAbilitiesCount = 0;
        readyToSpawn = false;

        // Caching for Infinite Coroutine to reduce GC
        waitTime = new WaitForSeconds(timeInterval);
        waitUntilReadyToSpawn = new WaitUntil(() => readyToSpawn == true);

        xPosMin = groundGameObject.bounds.min.x + xOffset;
        xPosMax = groundGameObject.bounds.max.x - xOffset;
        zPosMin = groundGameObject.bounds.min.z + zOffset;
        zPosMax = groundGameObject.bounds.max.z - zOffset;

        // Creating a new int array and populating with prefix sum of Spawn Priority values.
        prefixSumSpawnPriority = new int[GameDataSource.Instance.SortedAbilityArray.Length];
        prefixSumSpawnPriority[0] = GameDataSource.Instance.SortedAbilityArray[0].SpawnPriority;
        for (int i = 1; i < prefixSumSpawnPriority.Length; i++)
        {
            prefixSumSpawnPriority[i] = prefixSumSpawnPriority[i - 1] + GameDataSource.Instance.SortedAbilityArray[i].SpawnPriority;
        }

        #region Prerequisite for GetAbilityTypeEnum method with complexity O(n).
        // Declare 'int totalSpawnPriority' above Start.
        /*foreach (AbilityDescriptionSO ability in GameDataSource.Instance.AbilityDataByType.Values)
        {
            totalSpawnPriority += ability.SpawnPriority;
        }*/
        #endregion

        // Subscribing to events.
        abilityTypeUpdatedChannel.OnVoidEventRaised += ReadyToSpawn;
        abilityToDisableChannel.OnUIntEventRaised += UnspawnAbility;
        
        // Starting the coroutine that spawns new ability periodically.
        spawnCoroutine = SpawnCountdown();
        StartCoroutine(spawnCoroutine);
    }

    private void OnDestroy()
    {
        if (spawnCoroutine != null)
        {
            StopCoroutine(spawnCoroutine);
        }
    }

    #region Spawning Ability
    IEnumerator SpawnCountdown()
    {
        yield return new WaitForSeconds(5); // During development phase only. Remove this later.

        while (true)
        {
            yield return waitTime;

            if (activeAbilitiesCount < maxAbilitiesActive)
            {
                // Retrieving new spawn position.
                Vector3 newPosition = GetSpawnPosition();
                if (newPosition != Vector3.zero)
                {
                    // Requesting a new ability type & sending it to clients to update
                    // currentAbilityType in AbilitiesPooler.cs
                    abilitiesPooler.RpcUpdateCurrentAbility(GetAbilityTypeEnum());

                    // Halts the execution until the condition is met.
                    yield return waitUntilReadyToSpawn;

                    // Retrieving a gameobject of the ability type received above.
                    GetAbilityGameObject(newPosition);

                    readyToSpawn = false;
                }
            }
        }
    }

    Vector3 GetSpawnPosition()
    {
        int count = 0;
        while (count < maxPositionCheckerQuery)
        {
            float xPos = Random.Range(xPosMin, xPosMax);
            float zPos = Random.Range(zPosMin, zPosMax);
            Vector3 newPosition = new Vector3(xPos, 0, zPos);

            // Checks if any other GOs on layer, 'Non-Overlap' collide/overlap with the sphere of radius 'spawnPositionCheckerRadius'
            // at the new randomly generated position, 'newPosition'.
            Collider[] collisionCheck = new Collider[1];
            int overlapCount = Physics.OverlapSphereNonAlloc(newPosition, spawnPositionCheckerRadius, collisionCheck, collisionLayerMask); // Generates no garbage unlike OverlapSphere.
            if ((overlapCount == 0) && (newPosition != Vector3.zero))
            {
                return newPosition;
            }

            count += 1;
        }
        return Vector3.zero;
    }

    // Delete after making the tutorial.
    /*private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(newPosition, spawnPositionCheckerRadius);
    }*/

    // Implementation of lower bound from C++ for loot system based mechanics, i.e., some abilities will be spawned frequently and
    // others rarely depending upon SpawnPriority value.
    // Complexity of O(log(n))
    AbilityTypeEnum GetAbilityTypeEnum()
    {
        int roll = Random.Range(0, prefixSumSpawnPriority[prefixSumSpawnPriority.Length-1]);

        int mid;
        int low = 0;
        int high = prefixSumSpawnPriority.Length - 1;

        while (low < high)
        {
            mid = low + (high - low) / 2;
            if (roll >= prefixSumSpawnPriority[mid])
            {
                low = mid + 1;
            }
            else
            {
                high = mid;
            }
        }
        if (prefixSumSpawnPriority[low] <= roll)
        {
            low += 1;
        }
        return GameDataSource.Instance.SortedAbilityArray[low].AbilityTypeEnum;
    }

    // Complexity of O(n)
    /*AbilityTypeEnum GetAbilityTypeEnum()
    {
        int roll = Random.Range(0, totalSpawnPriority);

        for (int i = 0; i < GameDataSource.Instance.AbilityDataByType.Count; i++)
        {
            AbilityDescriptionSO temp = GameDataSource.Instance.SortedAbilityArray[i];
            
            roll -= temp.SpawnPriority;

            if (roll < 0)
            {
                return temp.AbilityTypeEnum;
            }
        }
        return AbilityTypeEnum.Armor; // Default return;
    }*/

    // Called when the currentAbilityType in AbilitiesPooler.cs has been updated by the clients.
    void ReadyToSpawn()
    {
        readyToSpawn = true;
    }

    void GetAbilityGameObject(Vector3 pos)
    {
        GameObject abilityGO = abilitiesPooler.GetFromPool(pos); // To-Do: Dynamically get position.
        if (abilityGO != null) // Can be null when the queue of the requested ability type is empty, ie, all the game objects of that type are already active in the scene.
        {
            NetworkServer.Spawn(abilityGO); // Spawning on clients. Tell server to send SpawnMessage, which will call SpawnHandler on client.
            activeAbilitiesCount += 1;
        }
    }
    #endregion

    #region Unspawning Ability
    void UnspawnAbility(uint abilityNetId)
    {
        GameObject go = NetworkIdentity.spawned[abilityNetId].gameObject;
        abilitiesPooler.PutBackInPool(go);
        NetworkServer.UnSpawn(go); // Unspawning on clients. Tell server to send ObjectDestroyMessage, which will call UnspawnHandler on client.
        activeAbilitiesCount -= 1;
    }
    #endregion
}
