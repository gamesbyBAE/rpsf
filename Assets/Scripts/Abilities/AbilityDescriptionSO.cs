using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Ability Description", menuName = "Ability")]
public class AbilityDescriptionSO : ScriptableObject
{
    [Tooltip("Ability Type Enum")]
    [SerializeField] AbilityTypeEnum abilityTypeEnum;

    [Tooltip("Minimum no of copies Object Pooler creates of this ability.")]
    [SerializeField] uint minCopies;

    [Tooltip("Determines how often the ability is to be spawned by the Object Pooler. Lower the value, rarer to spawn.")]
    [SerializeField] int spawnPriority;

    [Tooltip("Value that affects the stats of the player. Positive/Negative effect doesn't matter.")]
    [SerializeField] float effectValue;

    [Tooltip("How long (in seconds) the ability affects the player before they revert back to original state. 0 if instant effect.")]
    [SerializeField] float effectDuration;

    [Tooltip("How long (in seconds) the ability is available for players to pick it up. Disable it after mentioned duration if no one picks it.")]
    [SerializeField] float disableDuration;

    [Tooltip("Temporary: Material to be assigned to the spawned power ups.")]
    [SerializeField] Material material;

    [SerializeField] Sprite sprite;

    [Tooltip("Describing what the ability does. Only for Inspector purpose.")]
    [Multiline]
    [SerializeField] string description;

    #region Getters
    public AbilityTypeEnum AbilityTypeEnum { get { return abilityTypeEnum; } }
    public uint MinCopies { get { return minCopies; } }
    public int SpawnPriority { get { return spawnPriority; } }
    public float EffectValue { get { return effectValue; } }
    public float EffectDuration { get { return effectDuration; } }
    public Material AbilityMaterial { get { return material; } }
    public Sprite AbilitySprite { get { return sprite; } }
    #endregion
}
